scipy
pytest>=3.10
sphinx
sphinxcontrib-bibtex
git+https://github.com/funsim/moola.git@master
tensorflow
